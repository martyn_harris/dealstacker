var list_price,
    discount,
    shipping,
    delivery,
    VAT = 0.2,
    deal_price,
    deal_cost,
    deal_profit,
    margin=0.00,
    PERCENT = false;

function setDiscountType(){
    PERCENT = document.getElementById("discount-type").checked;
    update();
};

function getRate(from, to) {
    var script = document.createElement('script');
    script.setAttribute('src', "http://query.yahooapis.com/v1/public/yql?q=select%20rate%2Cname%20from%20csv%20where%20url%3D'http%3A%2F%2Fdownload.finance.yahoo.com%2Fd%2Fquotes%3Fs%3D"+from+to+"%253DX%26f%3Dl1n'%20and%20columns%3D'rate%2Cname'&format=json&callback=parseExchangeRate");
    document.body.appendChild(script);
}

function parseExchangeRate(data) {
    var name = data.query.results.row.name;
    var rate = parseFloat(data.query.results.row.rate, 10);
    console.log("Exchange rate " + name + " is " + rate);
}

function update(){
   list_price = clean(document.getElementById('list-price').value);
   discount = document.getElementById('discount').value;
   if (PERCENT == true){
       console.log("there's a %", discount);
       discount = clean(discount);
       vehicle_price = list_price - (list_price * (discount/100));
   } else {
       discount = clean(discount);
       console.log("there's no %", discount);
       vehicle_price = list_price - discount;
   };
   

   shipping = clean(document.getElementById('shipping-cost').value);

   delivery = clean(document.getElementById('shipping-cost').value);
   total_delivered_port = clean(document.getElementById('total-delivered-port').value);

   deal_cost = clean(document.getElementById('deal-cost').value)
   deal_profit = clean(document.getElementById('deal-profit').value);

   console.log("deal cost", $("#ex6SliderVal").text())
   
   calculate();
};

function setCost(){
   document.getElementById('deal-slider').value = formatNumber(document.getElementById('deal-cost').value); 
   update();  
};

function setSlider(){
   document.getElementById('deal-cost').value = formatNumber(document.getElementById('deal-slider').value);    
   update();
};

function clean(data){
   return data.replace(",", "").replace("%", "") * 1;
};

function calculate(){
    
    var VAT_back = (vehicle_price * VAT);
    margin = deal_cost - VAT_back;
    deal_price = document.getElementById('deal-price').value;
    document.getElementById('list-price').value = formatNumber(list_price);
    document.getElementById('discounted-price').value = formatNumber(vehicle_price);   
    document.getElementById('total-delivered-port').value = formatNumber(vehicle_price + delivery);
    document.getElementById('vat').value = formatNumber(VAT_back);

     

    document.getElementById('deal-price').value = formatNumber(Math.abs(total_delivered_port - VAT_back));
    
    document.getElementById('deal-slider').min = 0.00;
    document.getElementById('deal-slider').max = VAT_back;
//    document.getElementById('deal-slider-value').innerHTML = document.getElementById('deal-cost').value;
    
    var profit = (VAT_back) - (clean(document.getElementById('deal-cost').value));
    document.getElementById('deal-profit').value = formatNumber(profit);    
    console.log("profit", profit, VAT_back, parseInt(deal_cost.toString().replace(",", "")), VAT_back - parseInt(deal_cost.toString().replace(",", "")))
};

function formatNumber(number){
    //number-format the user input
    number = number.toString(); 
    var currency = parseFloat(number.replace(/,/g, ""))
                    .toFixed(2)
                    .toString()
                    .replace(/\B(?=(\d{3})+(?!\d))/g, ",");

    
    if (currency == NaN || currency == 'NaN'){
    	currency = 0.00;
        console.log("currency",number, currency)                   
    };
    
    return currency;
};
